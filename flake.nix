{
  description = "System for decoupling identities";

  inputs = {
    haskellNix.url = "github:input-output-hk/haskell.nix";
    nixpkgs.follows = "haskellNix/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {self, nixpkgs, flake-utils, haskellNix }:
    flake-utils.lib.eachDefaultSystem
      (system:
        with import nixpkgs { inherit system; };
        let
          overlays = [ haskellNix.overlay
                       (self: super: {
                         libsodium-vrf = stdenv.mkDerivation rec {
                           name = "libsodium-1.0.18";
                           src = fetchFromGitHub {
                             owner = "input-output-hk";
                             repo = "libsodium";
                             rev = "66f017f16633f2060db25e17c170c2afa0f2a8a1";
                             sha256 = "12g2wz3gyi69d87nipzqnq4xc6nky3xbmi2i2pb2hflddq8ck72f";
                           };
                           nativeBuildInputs = [ autoreconfHook ];
                           configureFlags = "--enable-static";
                           outputs = [ "out" "dev" ];
                           separateDebugInfo = stdenv.isLinux && stdenv.hostPlatform.libc != "musl";
                           enableParallelBuilding = true;
                           doCheck = true;
                         };
                         identityDecouplerProject =
                           self.haskell-nix.project' {
                             src = ./.;
                             compiler-nix-name = "ghc8107";
                             # This is used by `nix develop .` to open a shell for use with
                             # `cabal`, `hlint` and `haskell-language-server`
                             shell.tools = {
                               cabal = {};
                               hlint = {};
                             };
                             # Non-Haskell shell tools go here
                             shell.buildInputs = with pkgs; [
                               nixpkgs-fmt
                             ];
                             modules = [{
                               packages.cardano-crypto-praos.components.library.pkgconfig =
                                 lib.mkForce [ [ self.libsodium-vrf ] ];
                               packages.cardano-crypto-class.components.library.pkgconfig =
                                 lib.mkForce [ [ self.libsodium-vrf ] ];
                             }];
                           };
                       })
                     ];
          pkgs = import nixpkgs { inherit system overlays; inherit (haskellNix) config; };
          flake = pkgs.identityDecouplerProject.flake {};
    in flake // {
      # Built by `nix build .`
      defaultPackage = flake.packages."identity-decoupler:exe:identity-decoupler";
    });
}
