{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Crypto.TraceableRingSignature where

import Crypto.ECC.Edwards25519
import Crypto.Error
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import qualified Crypto.Number.Serialize.LE as LE
import Crypto.Number.ModArithmetic
import Data.ByteArray (ByteArrayAccess, Bytes)
import Crypto.Hash
import Crypto.Hash.Algorithms ()
import Data.Binary.Put as Bin
import Data.List
import Polysemy
import Polysemy.Error
import Crypto.PRandom
import Control.Monad

newtype RingMember = RingMember {
  ringMemberKey :: Point
                                } deriving (Eq, Show)
data SignatureTag = SignatureTag {
    tagRingMembers :: [RingMember]
  , tagIssue :: BS.ByteString
                                 } deriving (Eq, Show)

data MessageAndTag = MessageAndTag {
    msgTagMessage :: BS.ByteString
  , msgTagTag :: SignatureTag
                                   }

data KeyPair = KeyPair {
    pairPrivateKey :: Scalar
  , pairPublicKey :: Point
                       } deriving (Eq, Show)

data SignaturePerMember = SignaturePerMember {
     sigcj :: Scalar
   , sigzj :: Scalar
                                             }

data Signature = Signature {
    sigA1 :: Point
  , sigPerMember :: [SignaturePerMember]
                           }

ringMemberPut :: RingMember -> Put
ringMemberPut (RingMember p) = putByteString (pointEncode p)

signatureTagPut :: SignatureTag -> Either String Put
signatureTagPut (SignatureTag ringMembers _) | length ringMembers > 65535 = Left "Ring too large"
signatureTagPut (SignatureTag _ iss) | BS.length iss > 65535 = Left "Issue too long"
signatureTagPut (SignatureTag ringMembers iss) =
  Right (putWord16le (fromIntegral $ length ringMembers) >>
         foldMap ringMemberPut ringMembers >>
         putWord16le (fromIntegral $ BS.length iss) >>
         putByteString iss)

messageAndTagPut :: MessageAndTag -> Either String Put
messageAndTagPut (MessageAndTag msg _) | BS.length msg >= 2^(32 :: Int) = Left "Message too large"
messageAndTagPut (MessageAndTag msg tag) = do
  tagPut <- signatureTagPut tag
  pure $ tagPut >> putWord32le (fromIntegral $ BS.length msg) >> putByteString msg

pointPut :: Point -> Put
pointPut = putByteString . pointEncode

qPrime :: Integer
qPrime = 2^(252 :: Int) + 27742317777372353535851937790883648493

hashSomethingToPoint :: forall a. ByteArrayAccess a => a -> Point
hashSomethingToPoint input =
  let
    hashInteger = LE.os2ip (hash @a @SHA3_256 input)
    go i = case LE.i2ospOf @Bytes 32 (hashInteger + i) >>= (maybeCryptoError . pointDecode) of
             Just r -> r
             Nothing -> go (i + 1)
  in
    pointMulByCofactor (go 0)

hashSignatureTagToPoint :: SignatureTag -> Either String Point
hashSignatureTagToPoint t =
  (hashSomethingToPoint . LBS.toStrict . runPut) <$> signatureTagPut t

hashMessageAndTagToPoint :: MessageAndTag -> Either String Point
hashMessageAndTagToPoint t =
  (hashSomethingToPoint . LBS.toStrict . runPut) <$> messageAndTagPut t

hashMessageTaga0a1anbn :: MessageAndTag -> Point -> Point -> [(Point, Point)] -> Either String Integer
hashMessageTaga0a1anbn mt a0 a1 abN = do
  tagPut <- messageAndTagPut mt
  pure $ (`mod` qPrime) . LE.os2ip . hash @_ @SHA3_256 . LBS.toStrict . runPut $
         tagPut >> pointPut a0 >> pointPut a1 >> mapM_ (\(a, b) -> pointPut a >> pointPut b) abN

integerToScalar :: Integer -> Maybe Scalar
integerToScalar i = (maybeCryptoError . scalarDecodeLong) =<< (LE.i2ospOf @Bytes 32 i)

scalarToInteger :: Scalar -> Integer
scalarToInteger i = LE.os2ip $ scalarEncode @Bytes i


invertedScalarModQ :: Integer -> Maybe Scalar
invertedScalarModQ s = integerToScalar (inverseFermat s qPrime)

eitherStringToIO :: Either String a -> IO a
eitherStringToIO (Left msg) = fail msg
eitherStringToIO (Right a) = return a

pairToIndex :: SignatureTag -> KeyPair -> Maybe Int
pairToIndex (SignatureTag { tagRingMembers = m }) (KeyPair { pairPublicKey = k }) =
  snd <$> find (\(RingMember kr, _) -> kr == k) (zip m [1..])

zeroScalar :: Scalar
zeroScalar = maybe (error "Can't create zero rep") id $ integerToScalar 0

genKeyPair :: (PRandom `Member` r) => Sem r KeyPair
genKeyPair = do
  xi <- scalarGenerate
  return $ KeyPair xi (toPoint xi)

signingProtocol :: (PRandom `Member` r, Error String `Member` r) => MessageAndTag -> KeyPair -> Sem r Signature
signingProtocol mt@MessageAndTag { msgTagTag = tag }
                pair@(KeyPair { pairPrivateKey = xi }) = do
  let n = length (tagRingMembers tag)
  i <- maybe (throw "Key pair not member of ring") pure $ pairToIndex tag pair
  h <- fromEither $ hashSignatureTagToPoint tag
  a0 <- fromEither $ hashMessageAndTagToPoint mt
  let sigmai = xi `pointMul` h
  i' <- maybe (throw "Cannot invert i") pure $ invertedScalarModQ (fromIntegral i)
  let a1 = i' `pointMul` (sigmai `pointAdd` (pointNegate a0)) 
  wi <- scalarGenerate
  let ai = toPoint wi
  let bi = wi `pointMul` h
  zjs <- replicateM n scalarGenerate
  cjs <- replicateM n scalarGenerate
  let abN = map (\(j, zj, cj, RingMember yj) ->
                   if j == i then
                     (ai, bi)
                   else
                     let
                       -- Error should be impossible since max j already capped.
                       jScalar = maybe (error "Unexpected failure to convert Integer to Scalar") id
                                 (integerToScalar (fromIntegral j))
                       sigmaj = a0 `pointAdd` (jScalar `pointMul` a1)
                       aj = toPoint zj `pointAdd` (cj `pointMul` yj)
                       bj = (zj `pointMul` h) `pointAdd` (cj `pointMul` sigmaj)
                     in
                       (aj, bj)
                )
                (zip4 [1..n] zjs cjs (tagRingMembers tag))
  c <- fromEither $ hashMessageTaga0a1anbn mt a0 a1 abN
  let cjSum :: Integer
      cjSum = scalarToInteger (foldl' (\accum (j, cj) ->
                                         if j == i then accum else accum `scalarAdd` cj)
                               zeroScalar (zip [1..n] cjs))
  let ciInt = (c - cjSum) `mod` qPrime
  ci <- maybe (throw "Can't convert ci to Scalar") pure $ integerToScalar $ ciInt
  zi <- maybe (throw "Can't convert zi to Scalar") pure $ integerToScalar $ (scalarToInteger wi - scalarToInteger (ci `scalarMul` xi)) `mod` qPrime
  let memberSigs = map (\(j, cj, zj) -> if i == j then SignaturePerMember ci zi else SignaturePerMember cj zj)
                       (zip3 [1..n] cjs zjs)
  return Signature { sigA1 = a1, sigPerMember = memberSigs }

verificationProtocol :: (PRandom `Member` r, Error String `Member` r) => MessageAndTag -> Signature -> Sem r ()
verificationProtocol mt@MessageAndTag { msgTagTag = tag }
  Signature { sigA1 = a1, sigPerMember = memberSigs } = do
  h <- fromEither $ hashSignatureTagToPoint tag
  a0 <- fromEither $ hashMessageAndTagToPoint mt
  when (not $ pointHasPrimeOrder a1) $ throw "a1 doesn't have prime order"
  let n = length (tagRingMembers tag)
  when (length memberSigs /= n) $ throw "Signature size doesn't match tag size"
  abN <- forM (zip3 [1..n] memberSigs (tagRingMembers tag)) $
    \(i, SignaturePerMember ci zi, RingMember yi) -> do
      when (not $ pointHasPrimeOrder yi) $ throw "yi doesn't have prime order"
      iScalar <- maybe (throw "Can't convert i to scalar") pure $ integerToScalar (fromIntegral i)
      let sigmai = a0 `pointAdd` (iScalar `pointMul` a1)
      let ai = (toPoint zi) `pointAdd` (ci `pointMul` yi)
      let bi = (zi `pointMul` h) `pointAdd` (ci `pointMul` sigmai) 
      pure (ai, bi)
  let csum = foldl' (\accum (SignaturePerMember ci _) -> ci `scalarAdd` accum)
                    zeroScalar memberSigs
  hParamsInt <- fromEither $ hashMessageTaga0a1anbn mt a0 a1 abN
  hParams <- maybe (throw "Can't convert hParams to Scalar") pure $ integerToScalar hParamsInt
  when (hParams /= csum) $ throw "Signature not valid"
